import React from 'react';
import { useTable } from 'react-table';
import { capitalize } from './utils';

function Table({data, editRow, deleteRow}) {
  let columns = [];
  if (data[0]) {
    columns = Object.keys(data[0]).map(key => { return { Header: capitalize(key), accessor: d => d[key].toString() } });
  }
  console.log(columns);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  })

  // Render the UI for your table
  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
              <td>
                <button onClick={(e) => editRow(i, e)}>Edit</button>
              </td>
              <td>
                <button onClick={(e) => deleteRow(i, e)}>Delete</button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}
export default Table;
