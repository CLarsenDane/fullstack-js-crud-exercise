const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const employees = require('./data/employees.json');

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions));
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

function newCode() {
  lastCode = employees[employees.length - 1].code;
  letter = lastCode.substring(0, 1);
  number = parseInt(lastCode.substring(1), 10);
  number += 1;
  return letter + number;
}

app.get('/api/employees', (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees, null, 2));
})

app.put('/api/employees', (req, res, next) => {
  employee = req.body;

  const id = parseInt(employee.id, 10);
  const index = employees.map((employee) => employee.id).indexOf(id);
  employees.splice(index, 1, employee);

  res.setHeader('Content-Type', 'application/json');
  res.status(201);
  res.send(JSON.stringify({message: "Employee added."}, null, 2));
})

app.post('/api/employees', (req, res, next) => {
  newEmployee = req.body;
  newEmployee.id = employees.length + 1;
  newEmployee.code = newCode();
  employees.push(newEmployee);

  res.setHeader('Content-Type', 'application/json');
  res.status(201);
  res.send(JSON.stringify({message: "Employee added."}, null, 2));
})

app.delete('/api/employees/:id', (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  var removeIndex = employees.map((employee) => employee.id).indexOf(id);

  employees.splice(removeIndex, 1);

  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify({message: "Employee " + id + " removed."}, null, 2));
})

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))
