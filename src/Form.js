import React from 'react';

class EmployeeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.employee || {}
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  render() {
    console.log(this.state);
    return (
      <div className="form">
        <h2>Add Employee</h2>
        <form onSubmit={(e) => this.props.handleSubmit(this.state, e)}>
          <label>
            <span>Name:</span>
            <input name="name" type="text" defaultValue={this.state.name} onChange={this.handleInputChange}/>
          </label>
          <label>
            <span>Profession:</span>
            <input name="profession" type="text" defaultValue={this.state.profession} onChange={this.handleInputChange}/>
          </label>
          <label>
            <span>Color:</span>
            <input name="color" type="text" defaultValue={this.state.color} onChange={this.handleInputChange}/>
          </label>
          <label>
            <span>City:</span>
            <input name="city" type="text" defaultValue={this.state.city} onChange={this.handleInputChange}/>
          </label>
          <label>
            <span>Branch:</span>
            <input name="branch" type="text" defaultValue={this.state.branch} onChange={this.handleInputChange}/>
          </label>
          <label>
            <span>Assigned:</span>
            <input name="assigned" type="checkbox" checked={this.state.assigned} onChange={this.handleInputChange}/>
          </label>

          <input className="submit" type="submit" value="Submit" />
        </form>
      </div>
    )
  }
}

export default EmployeeForm;
