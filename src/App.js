import React from 'react';
import Table from './Table.js';
import EmployeeForm from './Form.js';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      create: false,
      employees: [],
      employee: null
    }
    this.createEmployee = this.createEmployee.bind(this);
    this.editEmployee = this.editEmployee.bind(this);
    this.updateEmployee = this.updateEmployee.bind(this);
    this.deleteEmployee = this.deleteEmployee.bind(this);
    this.showAddForm = this.showAddForm.bind(this);
  }
  componentDidMount = () => {
    this.fetchData();
  }
  fetchData() {
    fetch('http://localhost:8080/api/employees')
      .then(response => response.json())
      .then(employees => this.setState({ employees }))
  }
  deleteEmployee(row, e) {
    const id = this.state.employees[row].id;
    fetch(`http://localhost:8080/api/employees/${id}`, {
      method: 'DELETE',
    })
    .then(response => response.status)
    .then(status => this.fetchData())
  }
  createEmployee(employee, e) {
    fetch(`http://localhost:8080/api/employees`, {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(employee)
    })
    .then(response => response.status)
    .then(status => this.fetchData())

    this.setState({create: false});
  }
  updateEmployee(employee, e) {
    console.log('Update Employee');
    fetch(`http://localhost:8080/api/employees`, {
      method: 'PUT',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(employee)
    })
    .then(response => response.status)
    .then(status => this.fetchData())

    this.setState({edit: false});
  }
  editEmployee(row, e) {
    this.setState({edit: true, employee: this.state.employees[row]});
  }
  showAddForm() {
    this.setState({create: true, employee: null});
  }
  render() {
    const showForm = this.state.create || this.state.edit;
    const {
      employees,
      employee,
    } = this.state;

    // console.log(this.state);

    return (
      <div className="App">
        <h1>Plexxis Employees</h1>
        {this.state.create && <EmployeeForm employee={employee} handleSubmit={this.createEmployee}/>}
        {this.state.edit && <EmployeeForm employee={employee} handleSubmit={this.updateEmployee}/>}
        {!showForm && <div>
          <Table data={employees}
                 editRow={this.editEmployee}
                 deleteRow={this.deleteEmployee}
          />
          <button onClick={this.showAddForm}>Add Employee</button>
        </div>
        }
      </div>
    );
  }
}

export default App;
