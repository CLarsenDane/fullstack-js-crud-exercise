# Plexxis Interview Exercise

## Chris Larsen's Solution

This is my solution to the Plexxis interview exercise. I completed all of the required tasks, as well as 3 of the 4 bonus tasks. After spending 2 days teaching myself the Express library, I decided not to pursue learning how to add a relational database to an Express backend.

The Table component I created is basically a wrapper around a React Table, enabling me to use classes for the other components in the project, instead of being limited by the mechanisms of React Hooks. I should also note that I didn't know anything about React hooks prior to using this library. While the Table component was created just for this exercise, my natural tendency to design for multi-purpose did take hold. You'll notice that the variable names would make it easy to to reuse this component for another table of data.

I didn't focus much time on the CSS of the app, as I consider the JavaScript to be the more important part of this exercise. Therefore, I added enough CSS to make it intuitive and easy to use, without making it beautiful.
